#!/bin/bash
#Get servers list
set -f
string=$EC2_intancia_1
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}";do
	echo "Deploy project on server ${array[i]}"
	ssh ubuntu@${array[i]} "cd /home/ubuntu/my_drupal/ && git pull && pip install -r requirements.txt && python3 python_test.py"
done
