import requests
import json
import re

API_ENDPOINT = "https://apibenchmark.herokuapp.com/"
amount = 10
res = requests.get("http://whatismyip.org")
myIp = re.compile('(\d{1,3}\.){3}\d{1,3}').search(res.text).group()

data = {
  "url": myIp,
  "method": "GET",
  "usersAmount": amount,
  "requestsPerUser": 1,
  "body": ""
}
r = requests.post(url = API_ENDPOINT, data = data)
errors = json.loads(r.text)["result"]["errors"]
if errors == 0 :
    print("Test Passed")
else:
    raise Exception('The test failed with ' + str(errors) + ' of ' + str(amount))
